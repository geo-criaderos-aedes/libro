# *Optimización de la vigilancia y control de criaderos del Aedes aegypti utilizando computación móvil y sistemas de información geográfica*

**Trabajo Final de Grado presentado por:** Ignacio Luis Sánchez Kovács y Juan Angel Mendoza Barrientos

**Orientador:** Msc. Guillermo González

**Colaborador:** Lic. Edgar Sanabria

San Lorenzo (Paraguay), Noviembre de 2019